//
//  ProfileViewController.swift
//  UserBoards
//
//  Created by Claudiu Miron on 18/12/2019.
//  Copyright © 2019 Claudiu Miron. All rights reserved.
//

import UIKit


class ProfileViewController: UIViewController {
    
    var userId: Int?
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    
    private let profilePresenter =
        ProfilePresenter(profileService: ProfileService(userService: UserService(),
                                                        postService: PostService()))
    private var profileToShow: ProfileViewData?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let refresh = UIRefreshControl()
        refresh.addTarget(self,
                          action: #selector(didPullToRefresh),
                          for: .valueChanged)
        tableView.refreshControl = refresh
        
        tableView.dataSource = self
        profilePresenter.attachView(view: self)
        profilePresenter.getProfile(userId: userId!)
    }
    
    @objc func didPullToRefresh(refreshControl: UIRefreshControl) {
        profilePresenter.getProfile(userId: userId!)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let idx = tableView.indexPath(for: sender as! UITableViewCell)
        
        let postId = profilePresenter.idOfPost(at: idx!.row)
        
        let nextVC = segue.destination as! DiscussionViewController
        nextVC.postId = postId
    }
    
}

// MARK: - ProfileView

extension ProfileViewController: ProfileView {
    
    func startLoading() {
        tableView.refreshControl!.beginRefreshing()
    }
    
    func finishLoading() {
        tableView.refreshControl!.endRefreshing()
    }
    
    func setProfile(profile: ProfileViewData) {
        profileToShow = profile
        tableView.reloadData()
    }
    
}

// MARK: - UITableViewDataSource

extension ProfileViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView,
                   titleForHeaderInSection section: Int) -> String? {
        
        if section == 0 {
            return "Info"
            
        } else if section == 1 {
            return "Posts"
        }
        
        return nil
    }
    
    func tableView(_ tableView: UITableView,
                   numberOfRowsInSection section: Int) -> Int {
        
        if section == 0 {
            return 3 // name, username, email
            
        } else if section == 1, let profile = profileToShow {
            return profile.postListViewData.count
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView,
                   cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: UITableViewCell?
        
        if indexPath.section == 0 {
            cell = tableView.dequeueReusableCell(withIdentifier: "UserInfoCell",
                                                 for: indexPath)
            
            switch indexPath.row {
            case 0:
                cell!.textLabel!.text =
                "Name: \(profileToShow?.userViewData.name ?? "")"
            
            case 1:
                cell!.textLabel!.text =
                "User name: \(profileToShow?.userViewData.userName ?? "")"
            
            case 2:
                cell!.textLabel!.text =
                "E-mail: \(profileToShow?.userViewData.email ?? "")"
            
            default:
                cell!.textLabel!.text = ""
            }
            
            return cell!
        }
        
        // TODO: This feels awkward. Refactor.
        if indexPath.section != 1 {
            return cell!
        }
        
        cell = tableView.dequeueReusableCell(withIdentifier: "UserPostCell",
                                             for: indexPath)
        
        let post = profileToShow?.postListViewData[indexPath.row]
        
        cell!.textLabel!.text = post!.title
        cell!.detailTextLabel!.text = post!.body
        
        return cell!
    }
    
}
