//
//  UserListViewController.swift
//  UserBoards
//
//  Created by Claudiu Miron on 18/12/2019.
//  Copyright © 2019 Claudiu Miron. All rights reserved.
//


import UIKit


class UserListViewController: UIViewController {
    
    @IBOutlet weak var noUserLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    private let userListPresenter = UserListPresenter(userService: UserService())
    private var usersToShow = [UserViewData]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let refresh = UIRefreshControl()
        refresh.addTarget(self,
                          action: #selector(didPullToRefresh),
                          for: .valueChanged)
        tableView.refreshControl = refresh
        
        tableView.dataSource = self
        userListPresenter.attachView(view: self)
        userListPresenter.getUsers()
    }
    
    @objc func didPullToRefresh(refreshControl: UIRefreshControl) {
        userListPresenter.getUsers()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let cellIdx = tableView.indexPath(for: sender as! UITableViewCell)
        
        let nextVC = segue.destination as! ProfileViewController
        
        nextVC.userId = userListPresenter.idOfUser(at: cellIdx!.row)
    }
    
}

// MARK: - UserListView

extension UserListViewController: UserListView {
    
    func startLoading() {
        OperationQueue.main.addOperation {
            self.noUserLabel.isHidden = true
            self.tableView.refreshControl!.beginRefreshing()
        }
    }
    
    func finishLoading() {
        OperationQueue.main.addOperation {
           self.tableView.refreshControl!.endRefreshing()
        }
    }
    
    func setUsers(users: [UserViewData]) {
        OperationQueue.main.addOperation {
            self.usersToShow = users
            self.noUserLabel.isHidden = true
            self.tableView.reloadData()
        }
    }
    
    func setNoUsers() {
        OperationQueue.main.addOperation {
            self.noUserLabel.isHidden = false
        }
    }
    
}

// MARK: - UITableViewDataSource

extension UserListViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView,
                   numberOfRowsInSection section: Int) -> Int {
        return usersToShow.count
    }
    
    func tableView(_ tableView: UITableView,
                   cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UserCell",
                                                 for: indexPath)
        let user = usersToShow[indexPath.row]
        cell.textLabel!.text = user.name
        cell.detailTextLabel!.text = "@\(user.userName)"
        
        return cell
    }
    
}
