//
//  DiscussionViewController.swift
//  UserBoards
//
//  Created by Claudiu Miron on 22/12/2019.
//  Copyright © 2019 Claudiu Miron. All rights reserved.
//

import UIKit

class DiscussionViewController: UIViewController {
    
    public var postId: Int?
    
    @IBOutlet weak var tableView: UITableView!
    
    private var discussionToShow: DiscussionViewData?
    
    private let discussionPresenter =
        DiscussionPresenter(discussionService: DiscussionService(postService: PostService(),
                                                                 commentService: CommentService()))
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let refresh = UIRefreshControl()
        refresh.addTarget(self,
                          action: #selector(didPullToRefresh),
                          for: .valueChanged)
        tableView.refreshControl = refresh
        
        tableView.dataSource = self
        discussionPresenter.attachView(view: self)
        discussionPresenter.getDiscussion(postId: self.postId!)
    }
    
    @objc func didPullToRefresh(refreshControl: UIRefreshControl) {
        discussionPresenter.getDiscussion(postId: postId!)
    }
    
}

// MARK: - DiscussionView

extension DiscussionViewController : DiscussionView {
    
    func startLoading() {
        tableView.refreshControl!.beginRefreshing()
    }
    
    func finishLoading() {
        tableView.refreshControl!.endRefreshing()
    }
    
    func setDiscussion(discussion: DiscussionViewData) {
        self.discussionToShow = discussion
        self.tableView.reloadData()
    }
    
}

// MARK: - UITableViewDataSource

extension DiscussionViewController : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView,
                   numberOfRowsInSection section: Int) -> Int {
        return 2 + (discussionToShow?.commentListViewData.count ?? 0)
    }
    
    func tableView(_ tableView: UITableView,
                   cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellId = getCellIdentifier(for: indexPath)
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId,
                                                 for: indexPath)
        
        configureCell(cell, at: indexPath)
        
        return cell
    }
    
    func getCellIdentifier(for indexPath: IndexPath) -> String {
        if indexPath.row == 0 {
            return "TitleCell"
        }
        
        if indexPath.row == 1 {
            return "BodyCell"
        }
        
        return "CommentCell"
    }
    
    func configureCell(_ cell: UITableViewCell, at indexPath: IndexPath) {
        if indexPath.row == 0 {
            cell.textLabel!.text = discussionToShow?.postViewData.title
            return
        }
        
        if indexPath.row == 1 {
            cell.textLabel!.text = discussionToShow?.postViewData.body
            return
        }
        
        cell.textLabel!.text =
            discussionToShow?.commentListViewData[indexPath.row - 2].name
        cell.detailTextLabel!.text =
            discussionToShow?.commentListViewData[indexPath.row - 2].body
    }
    
}
