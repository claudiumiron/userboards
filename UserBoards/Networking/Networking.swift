//
//  Networking.swift
//  UserBoards
//
//  Created by Claudiu Miron on 23/12/2019.
//  Copyright © 2019 Claudiu Miron. All rights reserved.
//

import Foundation


class Networking {
    
    static func loadJSONFrom(urlString: String,
                             callBack: @escaping (Response?, Error?) -> Void) {
        let url = URL(string: urlString)!
        URLSession.shared.dataTask(with: url, completion: callBack).resume()
    }
    
}

struct Response {
    let data: Data
    let metadata: URLResponse?
}

extension URLSession {
    func dataTask(with url: URL,
                  completion: @escaping ((Response?, Error?) -> Void)) -> URLSessionDataTask {
        return dataTask(with: url, completionHandler: { (maybeData, maybeResponse, maybeError) in
            if let data = maybeData {
                completion(Response(data: data, metadata: maybeResponse), nil)
            } else if let error = maybeError {
                completion(nil, error)
            }
        })
    }
}
