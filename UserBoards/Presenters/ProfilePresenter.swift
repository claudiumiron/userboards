//
//  ProfilePresenter.swift
//  UserBoards
//
//  Created by Claudiu Miron on 18/12/2019.
//  Copyright © 2019 Claudiu Miron. All rights reserved.
//

class ProfilePresenter {
    
    private let profileService: ProfileService
    private weak var profileView: ProfileView?
    
    private var storedProfile: Profile?
    
    init(profileService: ProfileService) {
        self.profileService = profileService
    }
    
    func attachView(view: ProfileView) {
        profileView = view
    }
    
    func detachView() {
        profileView = nil
    }
    
    func getProfile(userId: Int) {
        profileView?.startLoading()
        
        profileService.getProfile(
            userId: userId,
            onSuccess: { [weak self] profile in
                self?.storedProfile = profile
                
                self?.profileView?.finishLoading()
                
                let userData = UserViewData(
                    userName: profile.user.username,
                    name: profile.user.name,
                    email: profile.user.email
                )
                
                let postListData = profile.posts.map({ post in
                    return PostViewData(title: post.title, body: post.body)
                })
                
                let profileViewData = ProfileViewData(
                    userViewData:userData,
                    postListViewData: postListData
                )
                
                self?.profileView?.setProfile(profile: profileViewData)
        },
            onFailure: { [weak self] (error) in
                self?.profileView?.finishLoading()
        })
        
    }
    
    func idOfPost(at index: Int) -> Int? {
        return storedProfile?.posts[index].id
    }
    
}
