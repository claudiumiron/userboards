//
//  UserListPresenter.swift
//  UserBoards
//
//  Created by Claudiu Miron on 18/12/2019.
//  Copyright © 2019 Claudiu Miron. All rights reserved.
//

class UserListPresenter {
    
    private let userService: UserService
    private weak var userListView: UserListView?
    
    private var storedUsers: [User]?
    
    init(userService: UserService) {
        self.userService = userService
    }
    
    func attachView(view: UserListView) {
        userListView = view
    }
    
    func detachView() {
        userListView = nil
    }
    
    func getUsers() {
        userListView?.startLoading()
        
        userService.getUsers(callBack: rxCompletion(
            onSuccess: { [weak self] users in
                self?.storedUsers = users
                
                self?.userListView?.finishLoading()
                
                let mappedUsers = users.map {user in
                    return UserViewData(
                        userName: user.username,
                        name: user.name,
                        email: user.email
                    )
                }
                self?.userListView?.setUsers(users: mappedUsers)
        },
            onError: { [weak self] error in
                self?.userListView?.finishLoading()
                
                self?.userListView?.setNoUsers()
        }))
    }
    
    func idOfUser(at index: Int) -> Int? {
        return storedUsers?[index].id
    }
    
}
