//
//  DiscussionPresenter.swift
//  UserBoards
//
//  Created by Claudiu Miron on 22/12/2019.
//  Copyright © 2019 Claudiu Miron. All rights reserved.
//

class DiscussionPresenter {
    
    private let discussionService: DiscussionService
    private weak var discussionView: DiscussionView?
    
    init(discussionService: DiscussionService) {
        self.discussionService = discussionService
    }
    
    func attachView(view: DiscussionView) {
        discussionView = view
    }
    
    func detachView() {
        discussionView = nil
    }
    
    func getDiscussion(postId: Int) {
        discussionView?.startLoading()
        
        discussionService.getDiscussion(
            postId: postId,
            onSuccess: { [weak self] discussion in
                self?.discussionView?.finishLoading()
                
                let postData = PostViewData(title: discussion.post.title,
                                            body: discussion.post.body)
                
                let commentListData = discussion.comments.map({ comment in
                    return CommentViewData(name: comment.name, body: comment.body)
                })
                
                let discussionData = DiscussionViewData(
                    postViewData: postData,
                    commentListViewData: commentListData
                )
                
                self?.discussionView?.setDiscussion(discussion: discussionData)
            },
            onFailure: { [weak self] error in
                self?.discussionView?.finishLoading()
            }
        )
        
    }
    
}
