//
//  Post.swift
//  UserBoards
//
//  Created by Claudiu Miron on 18/12/2019.
//  Copyright © 2019 Claudiu Miron. All rights reserved.
//

struct Post: Codable {
    let id: Int
    let userId: Int
    let title: String
    let body: String
}
