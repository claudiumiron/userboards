//
//  Comment.swift
//  UserBoards
//
//  Created by Claudiu Miron on 19/12/2019.
//  Copyright © 2019 Claudiu Miron. All rights reserved.
//

struct Comment: Codable {
    let id: Int
    let postId: Int
    let name: String
    let email: String
    let body: String
}
