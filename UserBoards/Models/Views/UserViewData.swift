//
//  UserViewData.swift
//  UserBoards
//
//  Created by Claudiu Miron on 18/12/2019.
//  Copyright © 2019 Claudiu Miron. All rights reserved.
//

struct UserViewData {
    let userName: String
    let name: String
    let email: String
}
