//
//  ProfileViewData.swift
//  UserBoards
//
//  Created by Claudiu Miron on 22/12/2019.
//  Copyright © 2019 Claudiu Miron. All rights reserved.
//

struct ProfileViewData {
    let userViewData: UserViewData
    let postListViewData: [PostViewData]
}
