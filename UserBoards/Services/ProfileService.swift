//
//  ProfileService.swift
//  UserBoards
//
//  Created by Claudiu Miron on 22/12/2019.
//  Copyright © 2019 Claudiu Miron. All rights reserved.
//

import Foundation

class ProfileService {
    
    enum ProfileError: Error {
        case NoResultFound
    }
    
    private let userService: UserService
    private let postService: PostService
    
    init(userService: UserService, postService: PostService) {
        self.userService = userService
        self.postService = postService
    }
    
    func getProfile(userId: Int,
                    onSuccess: @escaping (Profile) -> Void,
                    onFailure: @escaping (ProfileError) -> Void) {
        
        let dispatchGroup = DispatchGroup()
        
        var theUser: User?
        var thePosts: [Post]?
        
        dispatchGroup.enter()
        userService.getUser(id: userId) { user, error in
            theUser = user
            dispatchGroup.leave()
        }
        
        dispatchGroup.enter()
        postService.getPosts(userId: userId) { posts, error in
            thePosts = posts
            dispatchGroup.leave()
        }
        
        dispatchGroup.notify(queue: .main) {
            guard let user = theUser, let posts = thePosts else {
                onFailure(.NoResultFound)
                return
            }
            
            let profile = Profile(user: user, posts: posts)
            onSuccess(profile)
        }
        
    }
    
    
}
