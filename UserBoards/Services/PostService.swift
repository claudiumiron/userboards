//
//  PostService.swift
//  UserBoards
//
//  Created by Claudiu Miron on 18/12/2019.
//  Copyright © 2019 Claudiu Miron. All rights reserved.
//

import Foundation

class PostService {
    
    enum PostServiceError: Error {
        case CorruptServerData, ConnectionProblem
    }
    
    func getPost(postId: Int, callBack:@escaping (Post?, PostServiceError?) -> Void) {
        let urlString = "https://jsonplaceholder.typicode.com/posts?id=\(postId)"
        
        let completion: (Response?, Error?) -> Void = rxCompletion(
            onSuccess: { response in
                do {
                    let posts = try JSONDecoder().decode([Post].self,
                                                         from: response.data)
                    callBack(posts[0], nil)
                } catch {
                    callBack(nil, PostServiceError.CorruptServerData)
                }
        },
            onError: { error in
                callBack(nil, PostServiceError.ConnectionProblem)
        })
        
        Networking.loadJSONFrom(urlString: urlString, callBack: completion)
    }
    
    func getPosts(userId: Int, callBack:@escaping ([Post]?, PostServiceError?) -> Void) {
        let urlString = "https://jsonplaceholder.typicode.com/posts?userId=\(userId)"
        
        let completion: (Response?, Error?) -> Void = rxCompletion(
            onSuccess: { response in
                do {
                    let posts = try JSONDecoder().decode([Post].self,
                                                         from: response.data)
                    callBack(posts, nil)
                } catch {
                    callBack(nil, PostServiceError.CorruptServerData)
                }
        },
            onError: { error in
                callBack(nil, PostServiceError.ConnectionProblem)
        })
        
        Networking.loadJSONFrom(urlString: urlString, callBack: completion)
    }
    
}
