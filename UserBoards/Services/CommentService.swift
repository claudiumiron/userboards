//
//  CommentService.swift
//  UserBoards
//
//  Created by Claudiu Miron on 19/12/2019.
//  Copyright © 2019 Claudiu Miron. All rights reserved.
//

import Foundation

class CommentService {
    
    enum CommentServiceError: Error {
        case CorruptServerData, ConnectionProblem
    }
    
    func getComments(postId: Int, callBack:@escaping ([Comment]?, CommentServiceError?) -> Void) {
        let urlString = "https://jsonplaceholder.typicode.com/comments?postId=\(postId)"
        
        let completion: (Response?, Error?) -> Void = rxCompletion(
            onSuccess: { response in
                do {
                    let comments = try JSONDecoder().decode([Comment].self,
                                                         from: response.data)
                    callBack(comments, nil)
                } catch {
                    callBack(nil, CommentServiceError.CorruptServerData)
                }
        },
            onError: { error in
                callBack(nil, CommentServiceError.ConnectionProblem)
        })
        
        Networking.loadJSONFrom(urlString: urlString, callBack: completion)
    }
    
}
