//
//  UserService.swift
//  UserBoards
//
//  Created by Claudiu Miron on 18/12/2019.
//  Copyright © 2019 Claudiu Miron. All rights reserved.
//

import Foundation

class UserService {
    
    enum UserServiceError: Error {
        case CorruptServerData, ConnectionProblem
    }
    
    func getUsers(callBack:@escaping ([User]?, UserServiceError?) -> Void) {
        let urlString = "https://jsonplaceholder.typicode.com/users"
        
        let completion: (Response?, Error?) -> Void = rxCompletion(
            onSuccess: { response in
                do {
                    let users = try JSONDecoder().decode([User].self,
                                                        from: response.data)
                    callBack(users, nil)
                } catch {
                    callBack(nil, UserServiceError.CorruptServerData)
                }
        },
            onError: { error in
                callBack(nil, UserServiceError.ConnectionProblem)
        })
        
        Networking.loadJSONFrom(urlString: urlString, callBack: completion)
        
        /*
        let users = [User(id: -1,
                          username: "what",
                          name: "John Doe",
                          email: "what@lilwayne.com"),
                     User(id: -2,
                          username: "is",
                          name: "Tailor Tim",
                          email: "little@timmytailor.org"),
                     User(id: -3,
                          username: "happening",
                          name: "John Wick",
                          email: "sad@puppydea.th"),
                     User(id: -4,
                          username: "here",
                          name: "Wizard of Oz",
                          email: "man@curtains.oz")]
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
            callBack(users)
        }
        */
    }
    
    func getUser(id: Int, callBack:@escaping (User?, Error?) -> Void) {
        
        let urlString = "https://jsonplaceholder.typicode.com/users?id=\(id)"
        
        let completion: (Response?, Error?) -> Void = rxCompletion(
            onSuccess: { response in
                do {
                    let users = try JSONDecoder().decode([User].self,
                                                         from: response.data)
                    callBack(users[0], nil)
                } catch {
                    callBack(nil, UserServiceError.CorruptServerData)
                }
        },
            onError: { error in
                callBack(nil, UserServiceError.ConnectionProblem)
        })
        
        Networking.loadJSONFrom(urlString: urlString, callBack: completion)
    }
    
}
