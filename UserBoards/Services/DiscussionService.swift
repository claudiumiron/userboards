//
//  DiscussionService.swift
//  UserBoards
//
//  Created by Claudiu Miron on 22/12/2019.
//  Copyright © 2019 Claudiu Miron. All rights reserved.
//

import Foundation

class DiscussionService {
    
    enum DiscussionError: Error {
        case NoResultFound
    }
    
    private let postService: PostService
    private let commentService: CommentService
    
    init(postService: PostService, commentService: CommentService) {
        self.postService = postService
        self.commentService = commentService
    }
    
    func getDiscussion(postId: Int,
                       onSuccess: @escaping (Discussion) -> Void,
                       onFailure: @escaping (DiscussionError) -> Void) {
        
        let dispatchGroup = DispatchGroup()
        
        var thePost: Post?
        var theComments: [Comment]?
        
        dispatchGroup.enter()
        postService.getPost(postId: postId) { post, error in
            thePost = post
            dispatchGroup.leave()
        }
        
        dispatchGroup.enter()
        commentService.getComments(postId: postId) { comments, error in
            theComments = comments
            dispatchGroup.leave()
        }
        
        dispatchGroup.notify(queue: .main) {
            guard let post = thePost, let comments = theComments else {
                onFailure(.NoResultFound)
                return
            }
            
            let discussion = Discussion(post: post, comments: comments)
            onSuccess(discussion)
        }
        
    }
    
}
