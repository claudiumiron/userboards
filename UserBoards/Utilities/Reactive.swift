//
//  Reactive.swift
//  UserBoards
//
//  Created by Claudiu Miron on 23/12/2019.
//  Copyright © 2019 Claudiu Miron. All rights reserved.
//

import Foundation

enum ReactiveError: Error {
    case NoIdeaWhy
}

func rxCompletion<R>(onSuccess: @escaping (R) -> Void,
                     onError: @escaping (Error) -> Void) -> ((R?, Error?) -> Void) {
    return { (maybeResult, maybeError) in
        if let result = maybeResult {
            onSuccess(result)
        } else if let error = maybeError {
            onError(error)
        } else {
            onError(ReactiveError.NoIdeaWhy)
        }
    }
}
