//
//  ProfileView.swift
//  UserBoards
//
//  Created by Claudiu Miron on 18/12/2019.
//  Copyright © 2019 Claudiu Miron. All rights reserved.
//

import Foundation

protocol ProfileView: NSObjectProtocol {
    func startLoading()
    func finishLoading()
    func setProfile(profile: ProfileViewData)
}
