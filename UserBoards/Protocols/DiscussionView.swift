//
//  DiscussionView.swift
//  UserBoards
//
//  Created by Claudiu Miron on 22/12/2019.
//  Copyright © 2019 Claudiu Miron. All rights reserved.
//

import Foundation

protocol DiscussionView: NSObjectProtocol {
    func startLoading()
    func finishLoading()
    func setDiscussion(discussion: DiscussionViewData)
}
