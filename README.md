# Userboards

## About

This is my submission to a coding test that Codegile provided.
It consists of 3 screens:

- User list
- User profile: user info + post list (also an image placeholder; don't know why I did it, but hey, it fills some space)
- Post details: title, body, comments


## Known issues

- The way it looks. **Dear God.**
- Every screen has a pull-to-refresh feature, which also doubles as an activity indicator. Except for the first screen; for some reason (only known to Apple staff, I suspect), it doesn't show the activity indicator when you open the app.
- The UI probably looks terrible on iPhone landscape/on iPads. It was developed solely for iPhone, portrait.

## Disclaimer(s)

- This app was tested only on an iPhone 5s simulator (iOS 12.1).  
- This app was developed on a 2011 MacBook Pro.
- This app was developed "quickly" (quotes because my MBP is anything but fast), sloppily and somewhat angrily; I can think of some optimisations for the code off the top of my head (and probably more, if given time)

## Final thoughts

My MacBook is so slow it should ride the short bus.